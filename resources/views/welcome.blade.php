<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>B2G CMS</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        @if (Route::has('mailer'))
            <div class="b2g-cms-mailer">
                <h1>Mail gesendet!</h1>
                <p>Bitte überprüfen Sie Ihr Postfach.</p>
            </div>
        @endif

        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Docs</a>
                    <a href="{{ url('/api/v3/') }}">REST API ROUTES</a>
                </div>
            </div>
        </div>
        <div class="b2g-outer-wrapper">
            <header class="b2g-header">
                <figure class="b2g-logo-wrapper">
                    <img src="" alt="CMS Logo" width="200" height="200" />
                    <figcaption class="b2g-logo-text">
                        CMS Tryouts
                    </figcaption>
                </figure>
            </header>
            <nav class="b2g-nav">
                <ul class="b2g-nav-list">
                    <li class="b2g-nav-list-item">
                        <a href="/">
                            Home <i class="fa fa-home fa-fw"></i>
                        </a>
                    </li>
                    <li class="b2g-nav-list-item">
                        <a href="/pagebuilder">
                            Pagebuilder <i class="fa fa-edit fa-fw"></i>
                        </a>
                    </li>
                    <li class="b2g-nav-list-item">
                        <a href="/logout">
                            Logout <i class="fa fa-sign-out fa-fw"></i>
                        </a>
                    </li>
                </ul>
            </nav>
            <div class="b2g-inner-wrapper">
                <div class="b2g-headline-wrapper">
                    <h1 class="b2g-headline"></h1>
                </div>
            </div>
            <footer class="b2g-footer">
                <ul class="b2g-footer-list">
                    <li class="b2g-footer-list-item">
                        <a href="/wiki">
                            Wiki <i class="fa fa-book fa-fw"></i>
                        </a>
                    </li>
                    <li class="b2g-footer-list-item">
                        <a href="/agb">
                            AGB <i class="fa fa-file-alt fa-fw"></i>
                        </a>
                    </li>
                    <li class="b2g-footer-list-item">
                        <a href="/impressum">
                            Impressum <i class="fa fa-id-card fa-fw"></i>
                        </a>
                    </li>
                </ul>
            </footer>
        </div>
    </body>
</html>
