<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostsController
{
    public function store(Request $request)
    {
        // $this->checkInternetConnection();

        $insertRecord = $this->pfb->insertRecord([
            'title' => $request->title,
            'body' => $request->body,
            'date' => now()->toDateTimeString()
        ], true);

        return response()->json([
            'connected' => true,
            'post' => $insertRecord
        ]);
    }
}
