<?php
namespace CMS\v3\Page;

use CMS\CMS_Page\CMS_Page as CMSPage;

/**
 * Class Page
 *
 * @package CMS\v3\Page
 */
class Page {
    private $action;
    private $get_params;
    private $post_params;

    /**
     * Page constructor.
     *
     * @param $action
     * @param $get
     * @param $post
     */
    public function __construct($action, $get, $post) {
        $this->action = $action;
        $this->get_params = $get;
        $this->post_params = $post;
    }

    /**
     * @return false|string
     */
    public function rest_response() {
        if( !isset($this->action) || empty($this->action) ) {
            return json_encode([
                'response' => 'Define the ressource to query. No post_type for given slug',
                'success' => false,
            ]);
        } else if( $this->action === 'get' && isset($this->get_params) && !empty($this->get_params) ) {
            return json_encode([
                'response' => $this->get_rest_call(),
                'success' => true,
            ]);
        } else if( $this->action === 'delete' && isset($this->get_params) && !empty($this->get_params) ) {
            return json_encode([
                'response' => $this->delete_rest_call(),
                'success' => true,
            ]);
        } else if( $this->action === 'put' && isset($this->post_params) && !empty($this->post_params) ) {
            return json_encode([
                'response' => $this->put_rest_call(),
                'success' => true,
            ]);
        } else if( $this->action === 'post' && isset($this->post_params) && !empty($this->post_params) ) {
            return json_encode([
                'response' => $this->post_rest_call(),
                'success' => true,
            ]);
        } else if( $this->action === 'update' && isset($this->post_params) && !empty($this->post_params) ) {
            return json_encode([
                'response' => $this->update_rest_call(),
                'success' => true,
            ]);
        } else {
            return json_encode([
                'response' => 'Define the method to query data',
                'success' => false,
            ]);
        }
    }

    /**
     * @return mixed
     */
    public function get_rest_call() {
        $pageController = new CMSPage();
        return $pageController->getPages();
    }

    /**
     * @return mixed
     */
    public function post_rest_call() {
        $pageController = new CMSPage();
        return $pageController->updatePage($this->get_params['id']);
    }

    /**
     * @return mixed
     */
    public function delete_rest_call() {
        $pageController = new CMSPage();
        return $pageController->deletePage($this->get_params['id']);
    }

    /**
     * @return mixed
     */
    public function put_rest_call() {
        $pageController = new CMSPage();
        return $pageController->insertPage($this->post_params);
    }

    /**
     * @return mixed
     */
    public function update_rest_call() {
        $pageController = new CMSPage();
        return $pageController->updatePage($this->post_params);
    }
}

$page = new Page('GET', $_GET, $_POST);
echo $page->rest_response();
