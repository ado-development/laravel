<?php
use CMS\v3\Page\Page;

ini_set('display_errors', true);

switch( $_GET['rest'] ) {
    case 'page':
        $page = new Page('GET', $_GET, $_POST);
        echo $page->rest_response();
        break;
    default:
        echo var_export($_GET, true);
        break;
}
