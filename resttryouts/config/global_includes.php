<?php
/**
 * Vars init
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/config/global_config.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/config/global_vars.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/config/db_config.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/config/mail_config.php';

/**
 * Class init
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/database/Database.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/mail/Mailer.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/controller/Login.php';
