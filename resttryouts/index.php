<?php
    ini_set('display_errors', true);
    include_once $_SERVER['DOCUMENT_ROOT'] . '/config/global_includes.php';
?>
<!doctype html>
<html lang="de">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?=TITLE?></title>
	<link rel="stylesheet" type="text/css" href="styles/main.css"/>
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
</head>
<body>
<div class="b2g-outer-wrapper">
    <header class="b2g-header">
        <figure class="b2g-logo-wrapper">
            <img src="<?=LOGO?>" alt="CMS Logo" width="200" height="200" />
            <figcaption class="b2g-logo-text">
                <?=TITLE?>
            </figcaption>
        </figure>
    </header>
    <nav class="b2g-nav">
        <ul class="b2g-nav-list">
            <li class="b2g-nav-list-item">
                <a href="/">
                    Home <i class="fa fa-home fa-fw"></i>
                </a>
            </li>
            <li class="b2g-nav-list-item">
                <a href="/pagebuilder">
                    Pagebuilder <i class="fa fa-edit fa-fw"></i>
                </a>
            </li>
            <li class="b2g-nav-list-item">
                <a href="/logout">
                    Logout <i class="fa fa-sign-out fa-fw"></i>
                </a>
            </li>
        </ul>
    </nav>
    <div class="b2g-inner-wrapper">
        <div class="b2g-headline-wrapper">
            <h1 class="b2g-headline"></h1>
        </div>
    </div>
    <footer class="b2g-footer">
        <ul class="b2g-footer-list">
            <li class="b2g-footer-list-item">
                <a href="/wiki">
                    Wiki <i class="fa fa-book fa-fw"></i>
                </a>
            </li>
            <li class="b2g-footer-list-item">
                <a href="/agb">
                    AGB <i class="fa fa-file-alt fa-fw"></i>
                </a>
            </li>
            <li class="b2g-footer-list-item">
                <a href="/impressum">
                    Impressum <i class="fa fa-id-card fa-fw"></i>
                </a>
            </li>
        </ul>
    </footer>
	<!--script type="text/javascript" src="js/main.js"></script-->
	<script type="text/javascript" src="js/jquery-main.js"></script>
</div>
</body>
</html>
