<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require $_SERVER['DOCUMENT_ROOT'] . '/assets/libs/PHPMailer-master/src/Exception.php';
require $_SERVER['DOCUMENT_ROOT'] . '/assets/libs/PHPMailer-master/src/PHPMailer.php';
require $_SERVER['DOCUMENT_ROOT'] . '/assets/libs/PHPMailer-master/src/SMTP.php';
require $_SERVER['DOCUMENT_ROOT'] . '/config/global_includes.php';

class Mailer {
    public function __construct($email, $fname, $lname, $message )
    {


        $this->from  = $email;
        $this->formFName = $fname;
        $this->fromLName = $lname;
        $this->message = $message;
        $this->header = $this->printHeader;
        $this->footer = $this->printFooter;

        $mail = new PHPMailer(true);

        try {
            //Server settings
            $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
            // $mail->isSMTP();                                            // Send using SMTP
            $mail->Host       = 'smtp.strato.de';                    // Set the SMTP server to send through
            $mail->SMTPAuth   = false;                                   // Enable SMTP authentication
            $mail->Username   = 'masp1994@gmx.de';                     // SMTP username
            $mail->Password   = '31Mms.94051008';                               // SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
            $mail->Port       = 587;


            $mail->addCustomHeader('MIME-Version', '1.0');
            $mail->addCustomHeader('Content-Type', 'text/html; charset=ISO-8859-1');

            //Recipients
            $mail->setFrom($this->from, $this->formFName . ' ' . $this->fromLName);
            $mail->addAddress('webmaster@b2g-dev.de', 'Admin B2G Developement');     // Add a recipient
            $mail->addReplyTo('noreply@b2g-dev.de', 'No Reply');

            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'Nachricht vom Kontaktformular';
            $mail->Body    =  $this->header . $this->message . $this->footer;
            $mail->send();
            return true;
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
            return false;
        }
    }

    public function printHeader() {
        $header = '<html>
        <body>
            <table rules="all" style="border-color: #666;" cellpadding="10">
                <tr style="background: #eee;">
                    <td>';
        return $header;
    }

    public function printFooter() {
        $footer = '</td></tr></body></html>';



        return $footer;
    }
}
