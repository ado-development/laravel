<?php

namespace CMS;

use CMS\CMS_Database as DB;

/**
 * Class CMS_Page
 *
 * @package CMS\CMS_Page
 */
class CMS_Page {
    /** @var string $table_name */
    private $table_name = "cms_b2g_page";

    /** @var $page_id CMS_Page ID */
    public $page_id;

    /** @var int $publicaton_id */
    public $publicaton_id;

    /** @var string $page_slug */
    public $page_slug;

    /** @var string $language */
    public $language;

    /** @var bool $active */
    public $active = false;

    /** @var bool $archive */
    public $archive = false;

    /** @var $edited_at */
    public $edited_at;

    /** @var $created_at */
    public $created_at;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'page',
        'primary' => 'page_id',
        'fields' => array(
            '$publicaton_id' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'size' => 11),
            '$page_slug' => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isString', 'size' => 255),
            '$language' => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isString', 'size' => 2),
            '$active' => array('type' => self::TYPE_BOOL, 'required' => true),
            '$archive' => array('type' => self::TYPE_BOOL, 'required' => true),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
        )
    );

    /**
     * constructor.
     *
     * @param null $page_id
     */
    public function __construct($page_id = null)
    {
        $this->page_id = $page_id;
    }

    /**
     * gets all pages in the database and
     * returns a json response of all pages
     *
     * @return mixed
    */
    public function getPages() {
        // Build query
        $sql = new DB();
        $pages = $sql->select('*', $this->table_name);

        return json_encode([
            'pages' => $pages,
            'success' => true
        ]);
    }

    /**
     * insert page with id in the database and
     * returns a json response of the page
     *
     * @return mixed
     */
    public function updatePage($data) {
        // Build query
        $sql = new DB();
        $page = $sql->update();

        return json_encode([
            'page' => $page,
            'success' => true
        ]);
    }

    /**
     * delete page with id in the database and
     * returns a json response of the page
     *
     * @return mixed
     */
    public function deletePage($page_id) {
        // Build query
        $sql = new DB();
        $page = $sql->delete();

        return json_encode([
            'page' => $page,
            'success' => true
        ]);
    }

    /**
     * delete page with id in the database and
     * returns a json response of the page
     *
     * @return mixed
     */
    public function insertPage($data) {
        // Build query
        $sql = new DB();
        $page = $sql->insert();

        return json_encode([
            'page' => $page,
            'success' => true
        ]);
    }
}
