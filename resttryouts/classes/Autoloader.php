<?php
namespace CMS;

use CMS\CMS_Database as DB;
use CMS\CMS_Autoloader as ALoader;

class Autoloader {
    public function __construct() {
        spl_autoload_register(array($this, 'load_class'));
    }

    public static function register() {
        new Autoloader();
    }

    public function load_class($class_name) {
        $file = 'classes/'.strtolower(str_replace('\\','\/',$class_name)).'.php';
      if(file_exists($file))
      {
        require_once($file);
      }
    }
}

ini_set('display_errors', true);

ALoader:register();

$db = new DB();
echo $db->get_connection();
