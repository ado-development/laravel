<?php
namespace CMS;

/**
 * Class Database
 */
class Database {

    /** @var string $dbhost */
    private $dbhost = DBHOST;

    /** @var string $dbuser */
    private $dbuser = DBUSER;

    /** @var string $dbpass */
    private $dbpass = DBPASS;

    /** @var string $db */
    private $db = DBNAME;

    /** @var null $connection */
    private $connection = null;

    /**
     * Database constructor.
     */
    public function __construct() {
        $this->open_connection();
        return $this->get_connection();
    }

    /**
     * open db connection
     */
    public function open_connection() {
        $this->connection = new mysqli($this->dbhost, $this->dbuser, $this->dbpass, $this->db) or die("Connect failed: %s\n". $this->connection -> error);
    }

    /**
     * @return null
     */
    public function close_connection() {
        return $this->connection->close();
    }

    /**
     * @return null
     */
    public function get_connection() {
        return $this->connection;
    }

    /**
     * @param      $field
     * @param      $tablename
     * @param null $id_field
     * @param null $compare
     * @param null $id
     * @param int  $max
     *
     * @return mixed
     */
    public function select( $field, $tablename, $id_field = null, $compare = null, $id = null, $max = 0 ) {
        $select = 'SELECT '. $field . ' FROM ' . $tablename;
        $where = ($id_field !== null ? 'WHERE ' . $id_field . ' ' . $compare . ' ' . $id : '1');
        $limit = ($max !== 0 ? ' LIMIT ' . $max : '');
        $sql = $select . $where . $where . $limit;
        return $this->connection->query($sql);
    }
}

Autoloader:load_class('Database');
