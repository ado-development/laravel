<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/api/v3/', function ($name) {
    echo var_export($name, true);
    return view('rest');
})->where('name', '[A-Za-z]+');

Route::post('/api/v3/', function () {
    return view('rest');
});

Route::get('/api/v3/page', function () {
    $pages = DB::table('cms_b2g_page')->get();
    $contents = json_encode([
        'object_name' => 'page',
        'page' => $pages,
    ]);

    $statusCode = 200;
    $value = 'application/json';
    $cookie = cookie('request', time(), 60);

    $response = Response::make($contents, $statusCode);
    $response->header('Content-Type', $value);
    $response->withCookie($cookie);
    return $response;
});

Route::get('/api/v3/search', function () {
    $contents = json_encode([
        'object_name' => 'search',
        'page' => 'test',
    ]);

    $statusCode = 200;
    $value = 'application/json';
    $cookie = cookie('request', time(), 60);

    $response = Response::make($contents, $statusCode);
    $response->header('Content-Type', $value);
    $response->withCookie($cookie);
    return $response;
});

Route::get('/pw/reset/', function () {
    echo var_export($_POST, true);
    $to_name = $_POST['to_name'];
    $to_email = $_POST['to_email'];
    $data = $_POST['data'];

    Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {
        $message->to($to_email, $to_name);
        $message->subject('Artisans Web Testing Mail');
        $message->from('FROM_EMAIL_ADDRESS', 'Artisans Web');
    });
    return view('emails.mail');
});
