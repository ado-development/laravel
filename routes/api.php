<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/v3/', function () {
    return view('rest');
});

Route::post('/v3/', function () {
    return view('rest');
});

Route::get('/items', function(Request $request) {
    $data = [
        [
            "id" => 7,
            "name" => "na like this",
            "description" => "",
            "created_at" => "2020-07-26T05:53:00.376501Z",
            "updated_at" => "2020-07-26T05:53:00.376501Z"
        ], [
            "id" => 5,
            "name" => "write a book",
            "description" => "hohoho",
            "created_at" => "2020-07-26T05:47:00.908706Z",
            "updated_at" => "2020-07-26T05:53:00.376501Z"
        ]
    ];
    return response()->json($data);
});

Route::get('/items/{id}', function(Request $request) {
    $data = [
        'id' => 1,
        'name' => "Swim across the River Benue",
        'description' => "ho ho ho",
        'created_at' => "2020-07-26T22:31:04.49683Z",
        'updated_at' => "2020-07-26T22:31:04.49683Z"
    ];
    return response()->json($data);
});

Route::post('/items', function(Request $request) {
    $data = [
        'id' => 1,
        'name' => "Swim across the River Benue",
        'description' => "ho ho ho",
        'created_at' => "2020-07-26T22:31:04.49683Z",
        'updated_at' => "2020-07-26T22:31:04.49683Z"
    ];
    return response()->json($data, 201);
});

Route::get('/test', function () {
    return response()->json(array('test' => csrf_token()));
});

Route::post('/test', function (Request $request) {
    echo var_export($request, true);
    $to_name = $request->input('name');
    $to_email = $request->input('value');
    return response()->json(array(
        'name' => $to_name,
        'value' => $to_email,
    ), 200);
});

Route::get('/v3/page', function () {
    $pages = DB::table('cms_b2g_page')->get();
    $contents = json_encode([
        'object_name' => 'page',
        'page' => $pages,
    ]);

    $statusCode = 200;
    $value = 'application/json';
    $cookie = cookie('request', time(), 60);

    $response = Response::make($contents, $statusCode);
    $response->header('Content-Type', $value);
    $response->withCookie($cookie);
    return $response;
});

Route::get('/pw/reset/', function () {
    echo var_export($_POST, true);
    $to_name = $_POST['to_name'];
    $to_email = $_POST['to_email'];
    $data = $_POST['data'];

    Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {
        $message->to($to_email, $to_name);
        $message->subject('Artisans Web Testing Mail');
        $message->from('FROM_EMAIL_ADDRESS', 'Artisans Web');
    });
    return view('emails.mail');
});
